CREATE TABLE "project" (
    "project_id" SERIAL NOT NULL,
    "project_name" VARCHAR(255) NOT NULL,
    "project_created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "project_updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "project_pkey" PRIMARY KEY ("project_id")
);

CREATE TABLE "task" (
    "task_id" SERIAL NOT NULL,
    "task_name" VARCHAR(255) NOT NULL,
    "task_description" VARCHAR NOT NULL,
    "task_status" BOOLEAN NOT NULL DEFAULT false,
    "task_created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "task_updated_at" TIMESTAMP(3) NOT NULL,
    "project_id" INTEGER NOT NULL,

    CONSTRAINT "task_pkey" PRIMARY KEY ("task_id")
);

CREATE INDEX "task_project_id_idx" ON "task"("project_id");

ALTER TABLE "task" ADD CONSTRAINT "task_project_id_fkey" FOREIGN KEY ("project_id") REFERENCES "project"("project_id") ON DELETE CASCADE ON UPDATE CASCADE;
