package models

import "time"

type CreateProject struct {
	Name string `json:"name" validate:"required,min=3,max=255"`
}

type UpdateProject struct {
	Name string `json:"name" validate:"required,min=3,max=255"`
}

type Project struct {
	ID        *int       `json:"id" db:"project_id"`
	Name      *string    `json:"name" db:"project_name"`
	CreatedAt *time.Time `json:"createdAt" db:"project_created_at"`
	UpdatedAt *time.Time `json:"updatedAt" db:"project_updated_at"`
	Tasks     []Task     `json:"tasks"`
}
