package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"log"
	"todo-golang/internal/handlers"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Не удалось прочитать переменные окружения!")
	}

	app := fiber.New()
	handlers.SetupHandlers(app)

	app.Listen(":3000")
}
