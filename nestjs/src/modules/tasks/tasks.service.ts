import { Injectable } from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { TasksRepository } from './tasks.repository';
import { TaskResponseDto } from '../../types/dto/task.type';

@Injectable()
export class TasksService {
  constructor(private readonly repository: TasksRepository) {}

  async create(createTaskDto: CreateTaskDto): Promise<TaskResponseDto | void> {
    return this.repository.createTask(createTaskDto);
  }

  async findAll(): Promise<TaskResponseDto[]> {
    return this.repository.getAllTasks();
  }

  async findAllForProject(projectId: number): Promise<TaskResponseDto[]> {
    return this.repository.getAllTasksForProject(projectId);
  }

  async update(
    id: number,
    updateTaskDto: UpdateTaskDto
  ): Promise<TaskResponseDto | void> {
    return this.repository.updateTask(id, updateTaskDto);
  }

  async toggle(id: number): Promise<TaskResponseDto | void> {
    return this.repository.toggleStatusInTask(id);
  }

  async remove(id: number): Promise<void> {
    return this.repository.removeTask(id);
  }
}
