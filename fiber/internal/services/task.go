package services

import (
	"todo-golang/internal/models"
	"todo-golang/internal/repositories"
)

type TaskService struct {
	repo *repositories.TaskRepository
}

func NewTaskService() *TaskService {
	return &TaskService{
		repo: repositories.NewTaskRepository(),
	}
}

func (s TaskService) CreateTask(dto models.CreateTask) (models.Task, error) {
	task, err := s.repo.CreateTask(dto)
	if err != nil {
		return models.Task{}, err
	}

	return task, nil
}

func (s TaskService) ToggleTaskStatusById(id int) (models.Task, error) {
	task, err := s.repo.ToggleTaskStatusById(id)
	if err != nil {
		return models.Task{}, err
	}

	return task, nil
}

func (s TaskService) GetAllTasks() ([]models.Task, error) {
	tasks, err := s.repo.GetAllTasks()
	if err != nil {
		return make([]models.Task, 0), err
	}

	return tasks, nil
}

func (s TaskService) GetAllTasksByProjectId(projectId int) ([]models.Task, error) {
	tasks, err := s.repo.GetAllTasksByProjectId(projectId)
	if err != nil {
		return make([]models.Task, 0), err
	}

	return tasks, nil
}

func (s TaskService) UpdateTaskById(id int, dto models.UpdateTask) (models.Task, error) {
	task, err := s.repo.UpdateTaskById(id, dto)
	if err != nil {
		return models.Task{}, err
	}

	return task, nil
}

func (s TaskService) DeleteTaskById(id int) error {
	err := s.repo.DeleteTaskById(id)
	if err != nil {
		return err
	}

	return nil
}
