package handlers

import (
	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"strconv"
	"todo-golang/internal/models"
	"todo-golang/internal/services"
)

type handlers struct {
	projectService *services.ProjectService
	taskService    *services.TaskService
}

func SetupHandlers(app *fiber.App) {
	api := app.Group("/api")
	v1 := api.Group("/v1")

	h := handlers{
		projectService: services.NewProjectService(),
		taskService:    services.NewTaskService(),
	}

	// Routes for projects
	projects := v1.Group("/projects")
	projects.Post("/", h.createProject)
	projects.Get("/", h.getAllProjects)
	projects.Get("/:id", h.getProjectById)
	projects.Patch(":id", h.updateProjectById)
	projects.Delete(":id", h.deleteProjectById)
	projects.Post(":id/close", h.closeProjectById)

	// Routes for tasks
	tasks := v1.Group("/tasks")
	tasks.Post("/", h.createTask)
	tasks.Post("/:id/toggle", h.toggleTaskStatusById)
	tasks.Get("/", h.getAllTasks)
	tasks.Get("/projects/:projectId", h.getAllTasksByProjectId)
	tasks.Patch("/:id", h.updateTaskById)
	tasks.Delete("/:id", h.deleteTaskById)
}

func (h handlers) createProject(ctx *fiber.Ctx) error {
	payload := models.CreateProject{}
	if err := ctx.BodyParser(&payload); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Ошибка во входных данных",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	validate := validator.New()
	errValidation := validate.Struct(payload)
	if errValidation != nil {
		errors := errValidation.(validator.ValidationErrors)
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    errors[0].Error(),
			StatusCode: fiber.StatusBadRequest,
		})
	}
	project, err := h.projectService.CreateNewProject(payload)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Не удалось создать проект",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	return ctx.Status(fiber.StatusCreated).JSON(project)
}

func (h handlers) getAllProjects(ctx *fiber.Ctx) error {
	projects, err := h.projectService.GetAllProjects()
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Не удалось получить проекты",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(projects)
}

func (h handlers) getProjectById(ctx *fiber.Ctx) error {
	projectIdStr := ctx.Params("id")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID проекта",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	project, err := h.projectService.GetProjectById(projectId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(project)
}

func (h handlers) updateProjectById(ctx *fiber.Ctx) error {
	projectIdStr := ctx.Params("id")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID проекта",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	payload := models.UpdateProject{}
	if err := ctx.BodyParser(&payload); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Ошибка валидации данных",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	validate := validator.New()
	errValidation := validate.Struct(payload)
	if errValidation != nil {
		errors := errValidation.(validator.ValidationErrors)
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    errors[0].Error(),
			StatusCode: fiber.StatusBadRequest,
		})
	}
	project, err := h.projectService.UpdateProjectById(projectId, payload)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(project)
}

func (h handlers) deleteProjectById(ctx *fiber.Ctx) error {
	projectIdStr := ctx.Params("id")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID проекта",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	err = h.projectService.DeleteProjectById(projectId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.SendStatus(fiber.StatusNoContent)
}

func (h handlers) closeProjectById(ctx *fiber.Ctx) error {
	projectIdStr := ctx.Params("id")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID проекта",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	project, err := h.projectService.CloseProjectById(projectId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(project)
}

func (h handlers) createTask(ctx *fiber.Ctx) error {
	payload := models.CreateTask{}
	if err := ctx.BodyParser(&payload); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Ошибка во входных данных",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	validate := validator.New()
	errValidation := validate.Struct(payload)
	if errValidation != nil {
		errors := errValidation.(validator.ValidationErrors)
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    errors[0].Error(),
			StatusCode: fiber.StatusBadRequest,
		})
	}
	task, err := h.taskService.CreateTask(payload)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(task)
}

func (h handlers) toggleTaskStatusById(ctx *fiber.Ctx) error {
	taskIdString := ctx.Params("id")
	taskId, err := strconv.Atoi(taskIdString)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID задачи",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	task, err := h.taskService.ToggleTaskStatusById(taskId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанная задача не найдена!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(task)
}

func (h handlers) getAllTasks(ctx *fiber.Ctx) error {
	tasks, err := h.taskService.GetAllTasks()
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Не удалось получить задачи!",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(tasks)
}

func (h handlers) getAllTasksByProjectId(ctx *fiber.Ctx) error {
	projectIdStr := ctx.Params("projectId")
	projectId, err := strconv.Atoi(projectIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID проекта",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	tasks, err := h.taskService.GetAllTasksByProjectId(projectId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанный проект не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(tasks)
}

func (h handlers) updateTaskById(ctx *fiber.Ctx) error {
	taskIdStr := ctx.Params("id")
	taskId, err := strconv.Atoi(taskIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID задачи",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	payload := models.UpdateTask{}
	if err := ctx.BodyParser(&payload); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Ошибка во входных данных",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	validate := validator.New()
	errValidation := validate.Struct(payload)
	if errValidation != nil {
		errors := errValidation.(validator.ValidationErrors)
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    errors[0].Error(),
			StatusCode: fiber.StatusBadRequest,
		})
	}
	task, err := h.taskService.UpdateTaskById(taskId, payload)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    "Указанная задача не существует!",
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.Status(fiber.StatusOK).JSON(task)
}

func (h handlers) deleteTaskById(ctx *fiber.Ctx) error {
	taskIdStr := ctx.Params("id")
	taskId, err := strconv.Atoi(taskIdStr)
	if err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(models.Error{
			Message:    "Неправильно указан ID задачи",
			StatusCode: fiber.StatusBadRequest,
		})
	}
	err = h.taskService.DeleteTaskById(taskId)
	if err != nil {
		return ctx.Status(fiber.StatusNotFound).JSON(models.Error{
			Message:    err.Error(),
			StatusCode: fiber.StatusNotFound,
		})
	}
	return ctx.SendStatus(fiber.StatusNoContent)
}
