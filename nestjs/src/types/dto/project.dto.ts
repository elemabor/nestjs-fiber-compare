import { TaskResponseDto } from './task.type';

export interface ProjectResponseDto {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
  tasks: TaskResponseDto[];
}
