package models

import "time"

type Task struct {
	ID          *int       `json:"id" db:"task_id"`
	Name        *string    `json:"name" db:"task_name"`
	Description *string    `json:"description" db:"task_description"`
	Status      *bool      `json:"status" db:"task_status"`
	CreatedAt   *time.Time `json:"createdAt" db:"task_created_at"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"task_updated_at"`
}

type CreateTask struct {
	Name        string `json:"name" validate:"required,min=3,max=255"`
	Description string `json:"description" validate:"required,min=3"`
	ProjectID   int    `json:"projectId" validate:"required,gt=0"`
}

type UpdateTask struct {
	Name        *string `json:"name" validate:"max=255"`
	Description *string `json:"description"`
}
