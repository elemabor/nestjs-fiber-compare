package repositories

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"os"
	"todo-golang/internal/models"
)

type TaskRepository struct {
	conn *pgx.Conn
}

func NewTaskRepository() *TaskRepository {
	conn, err := pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Не удалось подключить к БД: %v\n", err)
		panic(err)
	}
	return &TaskRepository{
		conn: conn,
	}
}

func (r TaskRepository) CreateTask(dto models.CreateTask) (models.Task, error) {
	row := r.conn.QueryRow(context.Background(), `
INSERT INTO task (task_name, task_description, task_updated_at, project_id) 
VALUES ($1, $2, now(), $3) 
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at`,
		dto.Name, dto.Description, dto.ProjectID)
	var task models.Task
	err := row.Scan(&task.ID, &task.Name, &task.Description, &task.Status, &task.CreatedAt, &task.UpdatedAt)
	if err != nil {
		return models.Task{}, fmt.Errorf("Не удалось создать задачу", err)
	}
	return task, nil
}

func (r TaskRepository) ToggleTaskStatusById(id int) (models.Task, error) {
	row := r.conn.QueryRow(context.Background(), `
UPDATE task SET task_status = NOT task_status, task_updated_at = now() WHERE task_id = $1
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at
`, id)
	var task models.Task
	err := row.Scan(&task.ID, &task.Name, &task.Description, &task.Status, &task.CreatedAt, &task.UpdatedAt)
	if err != nil {
		return models.Task{}, fmt.Errorf("Не удалось обновить задачу", err)
	}
	return task, nil
}

func (r TaskRepository) GetAllTasks() ([]models.Task, error) {
	rows, err := r.conn.Query(context.Background(), `
SELECT task_id, task_name, task_description, task_status, task_created_at, task_updated_at
FROM task
ORDER BY task_created_at DESC
`)
	if err != nil {
		return []models.Task{}, fmt.Errorf("Не удалось получить задачи", err)
	}
	var tasks []models.Task
	for rows.Next() {
		var task models.Task
		err := rows.Scan(&task.ID, &task.Name, &task.Description, &task.Status, &task.CreatedAt, &task.UpdatedAt)
		if err != nil {
			fmt.Printf("Ошибка сканирования строки %v", err)
		}
		if task.ID != nil {
			tasks = append(tasks, task)
		}
	}
	if len(tasks) == 0 {
		return make([]models.Task, 0), nil
	}
	return tasks, nil
}

func (r TaskRepository) GetAllTasksByProjectId(projectId int) ([]models.Task, error) {
	rows, err := r.conn.Query(context.Background(), `
SELECT task_id, task_name, task_description, task_status, task_created_at, task_updated_at
FROM task
WHERE project_id = $1
ORDER BY task_created_at DESC
`, projectId)
	if err != nil {
		return []models.Task{}, fmt.Errorf("Не удалось получить задачи", err)
	}
	var tasks []models.Task
	for rows.Next() {
		var task models.Task
		err := rows.Scan(&task.ID, &task.Name, &task.Description, &task.Status, &task.CreatedAt, &task.UpdatedAt)
		if err != nil {
			fmt.Printf("Ошибка сканирования строки %v", err)
		}
		if task.ID != nil {
			tasks = append(tasks, task)
		}
	}
	if len(tasks) == 0 {
		return make([]models.Task, 0), nil
	}
	return tasks, nil
}

func (r TaskRepository) UpdateTaskById(id int, dto models.UpdateTask) (models.Task, error) {
	var row pgx.Row
	if dto.Name != nil && dto.Description != nil {
		row = r.conn.QueryRow(context.Background(), `
UPDATE task SET task_name = $1, task_description = $2, task_updated_at = now()
WHERE task_id = $3
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at
`,
			dto.Name, dto.Description, id)
	} else if dto.Name == nil {
		row = r.conn.QueryRow(context.Background(), `
UPDATE task SET task_description = $1, task_updated_at = now()
WHERE task_id = $2
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at
`,
			dto.Description, id)
	} else if dto.Description == nil {
		row = r.conn.QueryRow(context.Background(), `
UPDATE task SET task_name = $1, task_updated_at = now()
WHERE task_id = $2
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at
`,
			dto.Name, id)
	} else {
		row = r.conn.QueryRow(context.Background(), `
UPDATE task SET task_updated_at = now()
WHERE task_id = $1
RETURNING task_id, task_name, task_description, task_status, task_created_at, task_updated_at
`, id)
	}
	var task models.Task
	err := row.Scan(&task.ID, &task.Name, &task.Description, &task.Status, &task.CreatedAt, &task.UpdatedAt)
	if err != nil {
		return models.Task{}, fmt.Errorf("Не удалось обновить задачу", err)
	}
	return task, nil
}

func (r TaskRepository) DeleteTaskById(id int) error {
	var taskId int
	row := r.conn.QueryRow(context.Background(), `SELECT task_id FROM task WHERE task_id = $1`, id)
	err := row.Scan(&taskId)
	if err != nil || taskId == 0 {
		return fmt.Errorf("Указанная задача не найдена!")
	}
	_, err = r.conn.Exec(context.Background(), `DELETE FROM task WHERE task_id = $1`, id)
	if err != nil {
		return fmt.Errorf("Не удалось удалить задачу!")
	}
	return nil
}
