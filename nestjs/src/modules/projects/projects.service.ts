import { Injectable } from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectResponseDto } from '../../types/dto/project.dto';
import { ProjectsRepository } from './projects.repository';

@Injectable()
export class ProjectsService {
  constructor(private readonly repository: ProjectsRepository) {}

  async create(
    createProjectDto: CreateProjectDto
  ): Promise<ProjectResponseDto | void> {
    return this.repository.createProject(createProjectDto);
  }

  async findAll(): Promise<ProjectResponseDto[]> {
    return this.repository.getAllProjects();
  }

  async findOne(id: number): Promise<ProjectResponseDto | void> {
    return this.repository.getProjectById(id);
  }

  async update(
    id: number,
    updateProjectDto: UpdateProjectDto
  ): Promise<ProjectResponseDto | void> {
    return this.repository.updateProject(id, updateProjectDto);
  }

  async remove(id: number): Promise<void> {
    return this.repository.removeProject(id);
  }

  async close(id: number): Promise<ProjectResponseDto | void> {
    return this.repository.completeProject(id);
  }
}
