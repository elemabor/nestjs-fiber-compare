import random
import string

from locust import HttpUser, TaskSet, task


def generate_random_string():
    return ''.join(random.choices(string.ascii_uppercase + string.digits, k=10))


class UserBehavior(HttpUser):
    my_projects = []

    @task(1)
    def create_project(self):
        new_project = {'name': generate_random_string()}
        with self.client.post('/api/v1/projects', catch_response=True, json=new_project, name='Создание проекта') as response:
            if response.status_code != 201:
                response.success()
                return
            project_id = response.json().get('id')
            self.my_projects.append(project_id)

    @task(10)
    def get_all_projects(self):
        with self.client.get('/api/v1/projects', catch_response=True, name='Получение всех проектов') as response:
            if response.status_code != 200:
                response.success()
                return

    @task(5)
    def get_project(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        with self.client.get(f'/api/v1/projects/{project_id}', catch_response=True, name='Получение проекта') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(2)
    def update_project(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        new_project_data = {'name': generate_random_string()}
        with self.client.patch(f'/api/v1/projects/{project_id}', catch_response=True, json=new_project_data, name='Обновление проекта') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(3)
    def close_tasks_in_project(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        with self.client.post(f'/api/v1/projects/{project_id}/close', catch_response=True, name='Выполнение всех задач в проекте') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(1)
    def delete_project(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        with self.client.delete(f'/api/v1/projects/{project_id}', catch_response=True, name='Удаление проекта') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(7)
    def create_task(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        new_task_data = {"name": generate_random_string(), "description": generate_random_string(), "projectId": project_id}
        with self.client.post('/api/v1/tasks', catch_response=True, json=new_task_data, name='Создание новой задачи') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(8)
    def get_all_tasks(self):
        with self.client.get('/api/v1/tasks', catch_response=True, name='Получение всех задач') as response:
            if response.status_code != 200:
                response.success()
                return

    @task(10)
    def get_all_projects_tasks(self):
        if len(self.my_projects) == 0:
            return
        project_id = random.choice(self.my_projects)
        with self.client.get(f'/api/v1/tasks/projects/{project_id}', catch_response=True, name='Получение всех задач для проекта') as response:
            if response.status_code in [400, 404]:
                response.success()

    @task(4)
    def toggle_task_status(self):
        with self.client.get('/api/v1/tasks', catch_response=True, name='Получение всех задач для проекта') as response:
            if response.status_code != 200:
                response.success()
                return
            tasks = response.json()
            task_id = random.choice(tasks).get('id')
            with self.client.post(f'/api/v1/tasks/{task_id}/toggle', name='Переключение статуса задачи') as response1:
                if response1.status_code in [400, 404]:
                    response1.success()

    @task(3)
    def update_task(self):
        with self.client.get('/api/v1/tasks', catch_response=True, name='Получение всех задач для проекта') as response:
            if response.status_code != 200:
                response.success()
                return
            tasks = response.json()
            task_id = random.choice(tasks).get('id')
            new_data = {"name": generate_random_string(), "description": generate_random_string()}
            with self.client.patch(f'/api/v1/tasks/{task_id}', catch_response=True, json=new_data, name='Обновление задачи') as response1:
                if response1.status_code in [400, 404]:
                    response1.success()

    @task(1)
    def delete_task(self):
        with self.client.get('/api/v1/tasks', catch_response=True, name='Получение всех задач для проекта') as response:
            if response.status_code != 200:
                response.success()
                return
            tasks = response.json()
            task_id = random.choice(tasks).get('id')
            with self.client.delete(f'/api/v1/tasks/{task_id}', catch_response=True, name='Удаление задачи') as response1:
                if response1.status_code in [400, 404]:
                    response1.success()
