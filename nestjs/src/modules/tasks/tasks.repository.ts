import { PrismaService } from '../../common/db/prisma.service';
import {
  BadRequestException,
  Injectable,
  NotFoundException
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TaskResponseDto } from '../../types/dto/task.type';
import { errorsHandler } from '../../common/filters/errors.filter';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async createTask(dto: CreateTaskDto): Promise<TaskResponseDto | void> {
    return this.prismaService.$transaction(async tx => {
      const project = await tx.project.findUnique({
        where: { id: dto.projectId }
      });
      if (!project) {
        throw new BadRequestException('Указанный проект не найден!');
      }
      const task = await tx.task
        .create({
          data: {
            name: dto.name,
            description: dto.description,
            projectId: dto.projectId
          }
        })
        .catch(errorsHandler);
      if (!task) {
        return;
      }
      return this.modelToDto(task);
    });
  }

  async getAllTasks(): Promise<TaskResponseDto[]> {
    const tasks = await this.prismaService.task.findMany({
      orderBy: {
        createdAt: 'desc'
      }
    });
    return tasks.map<TaskResponseDto>(task => {
      return this.modelToDto(task);
    });
  }

  async getAllTasksForProject(projectId: number): Promise<TaskResponseDto[]> {
    const tasks = await this.prismaService.task.findMany({
      where: { projectId },
      orderBy: {
        createdAt: 'desc'
      }
    });
    return tasks.map<TaskResponseDto>(task => {
      return this.modelToDto(task);
    });
  }

  async updateTask(
    id: number,
    dto: UpdateTaskDto
  ): Promise<TaskResponseDto | void> {
    const task = await this.prismaService.task
      .update({
        where: { id },
        data: {
          name: dto.name,
          description: dto.description
        }
      })
      .catch(errorsHandler);
    if (!task) {
      return;
    }
    return this.modelToDto(task);
  }

  async toggleStatusInTask(id: number): Promise<TaskResponseDto | void> {
    return this.prismaService.$transaction(async tx => {
      const task = await tx.task.findUnique({
        where: { id }
      });
      if (!task) {
        throw new NotFoundException('Указанная задача не найдена!');
      }
      const updatedTask = await tx.task
        .update({
          where: { id },
          data: {
            status: {
              set: !task.status
            }
          }
        })
        .catch(errorsHandler);
      if (!updatedTask) {
        return;
      }
      return this.modelToDto(updatedTask);
    });
  }

  async removeTask(id: number): Promise<void> {
    await this.prismaService.task
      .delete({
        where: { id }
      })
      .catch(errorsHandler);
  }

  private modelToDto(task: {
    id: number;
    name: string;
    description: string;
    status: boolean;
    createdAt: Date;
    updatedAt: Date;
    projectId: number;
  }): TaskResponseDto {
    return {
      id: task.id,
      name: task.name,
      description: task.description,
      status: task.status,
      createdAt: task.createdAt,
      updatedAt: task.updatedAt
    };
  }
}
