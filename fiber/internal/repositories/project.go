package repositories

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"os"
	"todo-golang/internal/models"
)

type ProjectRepository struct {
	conn *pgx.Conn
}

func NewProjectRepository() *ProjectRepository {
	conn, err := pgx.Connect(context.Background(), os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Fprintf(os.Stderr, "Не удалось подключить к БД: %v\n", err)
		panic(err)
	}
	return &ProjectRepository{
		conn: conn,
	}
}

func (r *ProjectRepository) CreateProject(dto models.CreateProject) (models.Project, error) {
	row := r.conn.QueryRow(context.Background(), `INSERT INTO project (project_name, project_updated_at) VALUES ($1, now()) RETURNING project_id`, dto.Name)
	var projectId int
	err := row.Scan(&projectId)
	if err != nil {
		return models.Project{}, fmt.Errorf("Не удалось создать проект", err)
	}
	rows, err := r.conn.Query(context.Background(), `
SELECT p.project_id,
       p.project_name,
       p.project_created_at,
       p.project_updated_at,
       t.task_id,
       t.task_name,
       t.task_description,
       t.task_status,
       t.task_created_at,
       t.task_updated_at
FROM project p
         LEFT JOIN task t on p.project_id = t.project_id
WHERE p.project_id = $1
ORDER BY p.project_created_at DESC, t.task_created_at DESC;
`, projectId)
	var project models.Project
	project.Tasks = make([]models.Task, 0)
	var tasks []models.Task
	for rows.Next() {
		var t models.Task
		err = rows.Scan(&project.ID, &project.Name, &project.CreatedAt, &project.UpdatedAt, &t.ID, &t.Name, &t.Description, &t.Status, &t.CreatedAt, &t.UpdatedAt)
		if err == nil {
			tasks = append(tasks, t)
		}
	}
	project.Tasks = tasks
	return project, nil
}

func (r *ProjectRepository) GetAllProjects() ([]models.Project, error) {
	rows, err := r.conn.Query(context.Background(), `
SELECT p.project_id,
       p.project_name,
       p.project_created_at,
       p.project_updated_at,
       t.task_id,
       t.task_name,
       t.task_description,
       t.task_status,
       t.task_created_at,
       t.task_updated_at
FROM project p
         LEFT JOIN task t on p.project_id = t.project_id
ORDER BY p.project_created_at DESC, t.task_created_at DESC;
`)
	if err != nil {
		return []models.Project{}, fmt.Errorf("Не удалось получить список проектов")
	}
	var projects []models.Project
	for rows.Next() {
		var p models.Project
		p.Tasks = make([]models.Task, 0)
		var t models.Task
		err = rows.Scan(&p.ID, &p.Name, &p.CreatedAt, &p.UpdatedAt, &t.ID, &t.Name, &t.Description, &t.Status, &t.CreatedAt, &t.UpdatedAt)
		if err != nil {
			fmt.Printf("Ошибка сканирования строки %s", err)
		}
		if len(projects) == 0 {
			projects = append(projects, p)
		}
		if projects[len(projects)-1].ID == p.ID {
			if t.ID != nil {
				projects[len(projects)-1].Tasks = append(projects[len(projects)-1].Tasks, t)
			}
		} else {
			projects = append(projects, p)
		}
	}
	return projects, nil
}

func (r *ProjectRepository) GetProjectById(id int) (models.Project, error) {
	rows, err := r.conn.Query(context.Background(), `
SELECT p.project_id,
       p.project_name,
       p.project_created_at,
       p.project_updated_at,
       t.task_id,
       t.task_name,
       t.task_description,
       t.task_status,
       t.task_created_at,
       t.task_updated_at
FROM project p
         LEFT JOIN task t on p.project_id = t.project_id
WHERE p.project_id = $1
ORDER BY p.project_created_at DESC, t.task_created_at DESC;
`, id)
	if err != nil {
		return models.Project{}, fmt.Errorf("Ошибка при выполнении запроса!")
	}
	var p models.Project
	p.Tasks = make([]models.Task, 0)
	for rows.Next() {
		var t models.Task
		err = rows.Scan(&p.ID, &p.Name, &p.CreatedAt, &p.UpdatedAt, &t.ID, &t.Name, &t.Description, &t.Status, &t.CreatedAt, &t.UpdatedAt)
		if err != nil {
			fmt.Printf("Ошибка сканирования строки %s", err)
		}
		if t.ID != nil {
			p.Tasks = append(p.Tasks, t)
		}
	}
	if p.ID == nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}
	return p, nil
}

func (r *ProjectRepository) UpdateProjectById(id int, dto models.UpdateProject) (models.Project, error) {
	_, err := r.GetProjectById(id)
	if err != nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}
	_, err = r.conn.Exec(context.Background(), `UPDATE project SET project_updated_at = now(), project_name = $1 WHERE project_id = $2`, dto.Name, id)
	if err != nil {
		return models.Project{}, err
	}
	return r.GetProjectById(id)
}

func (r *ProjectRepository) DeleteProjectById(id int) error {
	_, err := r.GetProjectById(id)
	if err != nil {
		return fmt.Errorf("Указанный проект не найден!")
	}
	_, err = r.conn.Exec(context.Background(), `DELETE FROM project WHERE project_id = $1`, id)
	if err != nil {
		return err
	}
	return nil
}

func (r *ProjectRepository) CloseProjectById(id int) (models.Project, error) {
	_, err := r.GetProjectById(id)
	if err != nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}
	_, err = r.conn.Exec(context.Background(), `UPDATE task SET task_status = true, task_updated_at = now() WHERE project_id = $1`, id)
	if err != nil {
		return models.Project{}, err
	}
	return r.GetProjectById(id)
}
