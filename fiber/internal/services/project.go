package services

import (
	"fmt"
	"todo-golang/internal/models"
	"todo-golang/internal/repositories"
)

type ProjectService struct {
	repo *repositories.ProjectRepository
}

func NewProjectService() *ProjectService {
	return &ProjectService{
		repo: repositories.NewProjectRepository(),
	}
}

func (s ProjectService) CreateNewProject(dto models.CreateProject) (models.Project, error) {
	project, err := s.repo.CreateProject(dto)
	if err != nil {
		return models.Project{}, err
	}

	return project, nil
}

func (s ProjectService) GetAllProjects() ([]models.Project, error) {
	projects, err := s.repo.GetAllProjects()
	if err != nil {
		return []models.Project{}, err
	}

	return projects, nil
}

func (s ProjectService) GetProjectById(id int) (models.Project, error) {
	project, err := s.repo.GetProjectById(id)
	if err != nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}

	return project, err
}

func (s ProjectService) UpdateProjectById(id int, dto models.UpdateProject) (models.Project, error) {
	project, err := s.repo.UpdateProjectById(id, dto)
	if err != nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}

	return project, err
}

func (s ProjectService) DeleteProjectById(id int) error {
	err := s.repo.DeleteProjectById(id)
	if err != nil {
		return fmt.Errorf("Указанный проект не найден!")
	}

	return nil
}

func (s ProjectService) CloseProjectById(id int) (models.Project, error) {
	project, err := s.repo.CloseProjectById(id)
	if err != nil {
		return models.Project{}, fmt.Errorf("Указанный проект не найден!")
	}

	return project, nil
}
