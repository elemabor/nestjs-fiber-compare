import {
  IsInt,
  IsNotEmpty,
  IsPositive,
  IsString,
  MaxLength,
  MinLength
} from 'class-validator';
import { Transform, TransformFnParams } from 'class-transformer';

export class CreateTaskDto {
  @IsString()
  @MaxLength(255)
  @MinLength(3)
  @IsNotEmpty()
  @Transform(({ value }: TransformFnParams) => value?.trim())
  name: string;

  @IsString()
  @MinLength(3)
  @Transform(({ value }: TransformFnParams) => value?.trim())
  description: string;

  @IsInt()
  @IsPositive()
  projectId: number;
}
