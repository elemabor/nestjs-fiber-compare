import { HttpException, HttpStatus } from '@nestjs/common';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime/library';

export function errorsHandler(err) {
  if (err instanceof PrismaClientKnownRequestError) {
    if (err.code == 'P2002') {
      throw new HttpException(
        err.meta.target
          ? `${err.meta.target} - должен быть уникальным`
          : 'запись уже существует',
        HttpStatus.BAD_REQUEST
      );
    } else if (err.code == 'P2003') {
      // понаблюдать за этой проверкой !!!
      throw new HttpException(
        `${err.meta.field_name} - есть связанные сущности`,
        HttpStatus.BAD_REQUEST
      );
    } else if (err.code == 'P2025') {
      throw new HttpException(`Запись не существует`, HttpStatus.NOT_FOUND);
    } else if (err.code == 'P2023') {
      throw new HttpException(
        `Неправильно переданы параметры. ${err.meta?.message}`,
        HttpStatus.BAD_REQUEST
      );
    }
  }

  if (err.code === 'InvalidRequest') {
    throw new HttpException('Файл не найден', HttpStatus.NOT_FOUND);
  } else if (err.code === 'NotFound') {
    throw new HttpException(
      'Произошла ошибка при загрузке файла',
      HttpStatus.NOT_FOUND
    );
  }

  console.error(err);
  throw new HttpException('Произошла ошибка', HttpStatus.INTERNAL_SERVER_ERROR);
}
