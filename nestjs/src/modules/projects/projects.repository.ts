import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../../common/db/prisma.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { ProjectResponseDto } from '../../types/dto/project.dto';
import { TaskResponseDto } from '../../types/dto/task.type';
import { errorsHandler } from '../../common/filters/errors.filter';
import { UpdateProjectDto } from './dto/update-project.dto';

@Injectable()
export class ProjectsRepository {
  constructor(private readonly prismaService: PrismaService) {}

  async createProject(
    dto: CreateProjectDto
  ): Promise<ProjectResponseDto | void> {
    const project = await this.prismaService.project
      .create({
        data: {
          name: dto.name
        },
        include: {
          tasks: {
            orderBy: {
              createdAt: 'desc'
            }
          }
        }
      })
      .catch(errorsHandler);
    if (!project) {
      return;
    }
    return this.modelToDto(project);
  }

  async getAllProjects(): Promise<ProjectResponseDto[]> {
    const projects = await this.prismaService.project.findMany({
      include: {
        tasks: {
          orderBy: {
            createdAt: 'desc'
          }
        }
      },
      orderBy: {
        createdAt: 'desc'
      }
    });
    return projects.map<ProjectResponseDto>(project => {
      return this.modelToDto(project);
    });
  }

  async getProjectById(id: number): Promise<ProjectResponseDto | void> {
    const project = await this.prismaService.project
      .findUniqueOrThrow({
        where: { id },
        include: {
          tasks: {
            orderBy: {
              createdAt: 'desc'
            }
          }
        }
      })
      .catch(errorsHandler);
    if (!project) {
      return;
    }
    return this.modelToDto(project);
  }

  async updateProject(
    id: number,
    dto: UpdateProjectDto
  ): Promise<ProjectResponseDto | void> {
    const project = await this.prismaService.project
      .update({
        where: { id },
        data: {
          name: dto.name
        },
        include: {
          tasks: {
            orderBy: {
              createdAt: 'desc'
            }
          }
        }
      })
      .catch(errorsHandler);
    if (!project) {
      return;
    }
    return this.modelToDto(project);
  }

  async completeProject(id: number): Promise<ProjectResponseDto | void> {
    return this.prismaService.$transaction(async tx => {
      const project = await tx.project.findUnique({
        where: { id }
      });
      if (!project) {
        throw new NotFoundException('Указанный проект не найден!');
      }
      await tx.task.updateMany({
        where: { projectId: id },
        data: { status: true }
      });
      const updatedProject = await tx.project.findUnique({
        where: { id },
        include: {
          tasks: {
            orderBy: {
              createdAt: 'desc'
            }
          }
        }
      });
      return this.modelToDto(updatedProject);
    });
  }

  async removeProject(id: number): Promise<void> {
    await this.prismaService.project
      .delete({
        where: { id }
      })
      .catch(errorsHandler);
  }

  private modelToDto(
    project: {
      tasks: {
        id: number;
        name: string;
        description: string;
        status: boolean;
        createdAt: Date;
        updatedAt: Date;
        projectId: number;
      }[];
    } & { id: number; name: string; createdAt: Date; updatedAt: Date }
  ): ProjectResponseDto {
    return {
      id: project.id,
      name: project.name,
      createdAt: project.createdAt,
      updatedAt: project.updatedAt,
      tasks: project.tasks.map<TaskResponseDto>(task => {
        return {
          id: task.id,
          name: task.name,
          description: task.description,
          status: task.status,
          createdAt: task.createdAt,
          updatedAt: task.updatedAt
        };
      })
    };
  }
}
